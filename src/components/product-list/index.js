import React from "react";
import ProductCard from "../product-card/index.js";
export default class ProductList extends React.Component {
  state = {
    products: [],
    loading: true,
    selectedCategory: "all",
  };

  componentDidMount() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "https://fakestoreapi.com/products");
    xhr.onload = () => {
      if (xhr.status === 200) {
        const data = JSON.parse(xhr.response);
        console.log(data);
        this.setState({ products: data, loading: false });
      }
    };
    xhr.send();
  }

  handleCategoryChange = (category) => {
    console.log(category);
    this.setState({ selectedCategory: category });
  };

  render() {
    const { products, loading, selectedCategory } = this.state;

    if (loading) {
      return (
        <div className="loader-box">
          <span className="loader"></span>
        </div>
      );
    }

    const filteredProducts =
      selectedCategory === "all"
        ? products
        : products.filter((product) => product.category === selectedCategory);
    return (
      <>
        <div className="category-buttons">
          <button
            onClick={() => this.handleCategoryChange("all")}
            className={selectedCategory === "all" ? "active" : "btn"}
          >
            All
          </button>
          <button
            onClick={() => this.handleCategoryChange("electronics")}
            className={selectedCategory === "electronics" ? "active" : "btn"}
          >
            Electronics
          </button>
          <button
            onClick={() => this.handleCategoryChange("jewelery")}
            className={selectedCategory === "jewelery" ? "active" : "btn"}
          >
            Jewelery
          </button>
          <button
            onClick={() => this.handleCategoryChange("men's clothing")}
            className={selectedCategory === "men's clothing" ? "active" : "btn"}
          >
            Men Clothing
          </button>
          <button
            onClick={() => this.handleCategoryChange("women's clothing")}
            className={
              selectedCategory === "women's clothing" ? "active" : "btn"
            }
          >
            Women Clothing
          </button>
        </div>
        <div className="product-list">
          {filteredProducts.map((product) => (
            <ProductCard key={product.id} product={product} />
          ))}
        </div>
      </>
    );
  }
}
