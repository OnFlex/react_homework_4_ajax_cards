import React from "react";
import generateStarRating from "../../methods";
export default class ProductCard extends React.Component {
  render() {
    const { title, price, description, image, rating } = this.props.product;
    return (
      <div className="product-card">
        <div className="product-img">
          <a href="###">
            <img src={image} alt={title} />
          </a>
        </div>
        <h3 className="product-title">{title}</h3>
        <p className="description">{description}</p>
        <div className="product-info">
          <p className="price">
            <span className="price-txt">Price:</span> ${price}
          </p>
          <p>Rating: {generateStarRating(rating.rate)}</p>
          <div className="product-info2">
            <button className="add-btn">Add to Cart</button>
            <p>Count: {rating.count}</p>
          </div>
        </div>
      </div>
    );
  }
}
