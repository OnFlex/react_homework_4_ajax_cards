import React from "react";
import ProductList from "../product-list";
export default class App extends React.Component {
  render() {
    return (
      <div>
        <h1 className="title">Product List</h1>
        <ProductList />
      </div>
    );
  }
}
