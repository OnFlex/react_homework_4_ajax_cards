export default function generateStarRating(rating) {
  const MaxNumberOfStars = 5;
  const currentRate = Math.floor(rating);
  const whiteStars = MaxNumberOfStars - currentRate;
  const yellowStarSymbol = "\u2B50";
  const whiteStarSymbol = "\u2606";
  const starsRow =
    yellowStarSymbol.repeat(currentRate) + whiteStarSymbol.repeat(whiteStars);
  return starsRow;
}
