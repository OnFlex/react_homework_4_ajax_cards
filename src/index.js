import React from "react";
import ReactDOM from "react-dom";
import  "./components/styles/style.css";
import App from "./components/app/index";

ReactDOM.render(<App/>, document.getElementById("root"));